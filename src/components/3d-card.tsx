import {ReactNode} from "react";
import './3d-card.css';
import {useWindowDimensions} from "../utils/windows";

export interface Card3DProps {
    children: ReactNode;
}

export function Card3D({children}: Card3DProps) {
    const wd = useWindowDimensions();
    const padding = wd.width > 600 ? 64 : 0;

    return (
        <div className='card'
            style={{
                width: wd.width - 2 * padding,
                height: wd.height - 2 * padding,
                color: "white",
                animation:"card3dbackground 30s infinite",
            }}
        >
            <div className='card-3d-content'>
                {children}
            </div>
        </div>
    );
}
