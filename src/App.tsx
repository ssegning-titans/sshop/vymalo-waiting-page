import React from 'react';
import './App.css';
import {Card3D} from "./components/3d-card";

function App() {
    return (
        <>
            <header>
                <div>
                    <h3 title="*CONTENT-PRETTY">SMILE</h3>
                </div>
                <div>
                    <h3 title="nombre d'article">ARTICLES</h3>
                </div>
                <div>
                    <h3 title="type de videos disponible">VIDEOS</h3>
                </div>
                <div>
                    <h3 title="nombre de clients">ALMANAC</h3>
                </div>
            </header>

            <main className='centered'>
                <Card3D>
                    <div>
                        <h4>Welcome in the funiest world</h4>
                        <h1>SEMA</h1>
                        <p>We have to play and always smile here...</p>
                        <hr/>
                        <p>
                            <a href='Experiences/Newyork/index.html' target='_blank' rel='noreferrer'>Experience in New
                                York with Cupcakes</a>
                        </p>
                    </div>
                </Card3D>
            </main>

            <footer>
                <div>
                    <h2 title="Merci pour l'astuce"> Privacy Policy</h2>
                    la police de notre site sera detaille ici!!!
                </div>
                <div>
                    <h2 title="Merci pour l'astuce"> Nos Services </h2>
                    Vous pouvez voir ici les services que nous proposons
                </div>
                <div>
                    <h2 title="Merci pour l'astuce"> Contact</h2>
                    <p>Vous avez ici les differents moyens de nous contacter!!!</p>
                </div>

            </footer>
        </>
    );
}

export default App;
