import {createInstance, MatomoProvider} from "@datapunt/matomo-tracker-react";
import {ReactNode} from "react";

const instance = createInstance({
    urlBase: 'https://matamo.ssegning.com',
    siteId: 2,
});

const MP: any = MatomoProvider;

interface TrackingProps {
    children: ReactNode;
}

export function Tracking({children}: TrackingProps) {
    return (
        <MP value={instance}>
            {children}
        </MP>
    );
}
