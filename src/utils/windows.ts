import {useEffect, useState} from 'react';

interface WindowDimensions {
    width: number;
    height: number;
}

interface WindowDimensionsProps {
    padding: number
}

export function getWindowDimensions(): WindowDimensions {
    if (typeof window === 'undefined') {
        return {width: 0, height: 0};
    }

    const {innerWidth: width, innerHeight: height} = window;
    return {
        width,
        height
    };
}

export function useWindowDimensions({padding}: WindowDimensionsProps = {padding: 0}): WindowDimensions {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    useEffect(() => {
        function handleResize() {
            setWindowDimensions(getWindowDimensions());
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return {
        width: windowDimensions.width - padding * 2,
        height: windowDimensions.height - padding * 2,
    };
}
